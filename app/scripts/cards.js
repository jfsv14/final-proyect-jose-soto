console.log('Cargando cards...');
const dataCards = [{
    "title": "Juegos de terror",
    "url_image":"https://i.blogs.es/f00f9c/visa/1366_2000.jpeg",
    "desc":"Los 22 mejores videojuegos de terror que puedes jugar ya",
    "cta":"Show More",
    "link":"https://www.vidaextra.com/listas/21-mejores-videojuegos-terror-que-puedes-jugar"
},

{
    "title": "Juegos de Aventura",
    "url_image":"https://elcomercio.pe/resizer/fRSYeip5elDUt-lJyhY-Kwc517M=/580x330/smart/filters:format(jpeg):quality(75)/arc-anglerfish-arc2-prod-elcomercio.s3.amazonaws.com/public/V5VNDGGQUNBVZDNMXRMJ77EB5Q.jpg",
    "desc":"Los mejores juegos de aventura, juegalos aqui",
    "cta":"Show More",
    "link":"https://as.com/meristation/plataformas/computadora_personal/top/videojuegos_aventura/"

},
{
    "title": "Juegos de Deportes",
    "url_image":"https://noticiasvideojuegos.com/wp-content/uploads/2019/08/1565417993_1280x720.jpg",
    "desc":"Los mejores videojuegos de Deportes que puedes jugar ",
    "cta":"Show More",
    "link":"https://www.alfabetajuega.com/lista/mejores-juegos-deportes-pc"
},
{
    "title": "Juegos clasicos",
    "url_image":"https://media.revistagq.com/photos/5ca5e998d71dd94b3995633c/16:9/w_1600%2Cc_limit/los_100_mejores_videojuegos_de_la_historia_8925.jpg",
    "desc":"Los mejores juegos clasicos de todos los tiempos que puedes jugar ya",
    "cta":"Show More",
    "link":"https://www.revistagq.com/noticias/tecnologia/galerias/los-100-mejores-videojuegos-de-la-historia/8951"
},
{
    "title": "Juegos de shooter",
    "url_image":"https://areajugones.sport.es/wp-content/uploads/2020/05/MejoresShooter.jpg",
    "desc":"Los mejores videojuegos de shooter para jugar ya",
    "cta":"Show More",
    "link":"https://vandal.elespanol.com/rankings/pc/shooter"
},
{
    "title": "Juegos de Autos",
    "url_image":"https://allgamersin.com/wp-content/uploads/2018/03/Gran-Turismo-Sport-cars.jpg",
    "desc":"Los mejores videojuegos de Autos para jugar ya",
    "cta":"Show More",
    "link":"https://movilforum.com/mejores-juegos-coches-pc/"
}];

(function () {
    let CARD = {
        init: function (){
        //console.log('card module was loaded');
        let _self = this;

        //llamamos las funciones
        this.insertData(_self);
        //this.eventHandler(_self);
    },
    
    eventHandler: function (_) {
        let arrayRefs = document.querySelectorAll('.accordion-title');

        for (let x = 0; x < arrayRefs.length; x++){
            arrayRefs[x].addEventListener('click', function(event){
                console.log('event', event);
                _self.showTab(event.target);
            });
        }
    },
    
    insertData: function (_self) {
        dataCards.map(function(item, index){
            document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
        });
    },


    tplCardItem: function (item, index) {
        return(`<div class='card-item' id="card-number-${index}">
        <img src="${item.url_image}"/>
        <div class="card-info">
          <p class='card-title'>${item.title}</p>
          <p class='card-desc'>${item.desc}</p>
          <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
          </div>
        </div>`)},
    }

    CARD.init();
    })();
